import Bases.HousesBase;
import Houses.*;
import Performence.*;

public class Main {

    public static void main(String[] args) {

        Apartment apartment = new Apartment(200, 2500, 3, 60);
        Apartment apartment2 = new Apartment(500, 200, 1, 60);
        Cottage cottage = new Cottage(300, 200, 5000, 200);
        Mansion mansion = new Mansion(500, 10000, 2, 500);

        HousesBase housesBase = new HousesBase();
        housesBase.addToBase(apartment);
        housesBase.addToBase(cottage);
        housesBase.addToBase(mansion);
        housesBase.addToBase(apartment2);

        Scene scene = new Scene();
        scene.chooseVariant();


    }
}
