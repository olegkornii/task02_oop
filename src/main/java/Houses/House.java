package Houses;

/**
 * Represent house that are renting
 */
public abstract class House {
    /**
     * The price of House
     */
    int price;
    /**
     * The range from house to university
     */
    int rangeToUniversity;
    /**
     * The area of house
     */
    int area;

    @Override
    public String toString() {
        return "Price = " + price + " Range to University = "
                + rangeToUniversity;
    }

    public int getPrice() {
        return price;
    }


    public int getRangeToUniversity() {
        return rangeToUniversity;
    }

    public int getArea() {
        return area;
    }

}
