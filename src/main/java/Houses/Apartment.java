package Houses;

/**
 * Represent kind of House
 * Have unique characteristic: floor
 */
public class Apartment extends House {
    /**
     * Number of floor, where the apartment are locate
     */
    private int floor;

    public Apartment(int p, int r, int f, int s) {
        super.price = p;
        super.rangeToUniversity = r;
        super.area = s;
        floor = f;
    }

    @Override
    public String toString() {
        return "Apartment : \n"
                + " Floor = " + floor
                + ",\n Price = " + price
                + "\n Range to University = " + rangeToUniversity
                + ",\n Area = " + area;
    }

    public int getFloor() {
        return floor;
    }

}
