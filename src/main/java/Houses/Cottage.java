package Houses;
/**
 * Represent kind of House
 * Have unique characteristic: Area of yard
 */
public class Cottage extends House {
    /**
     * Yard area of the house
     */
    private int areaOfYard;

    public Cottage(int p, int s, int r, int a) {
        super.price = p;
        super.rangeToUniversity = r;
        super.area = s;
        areaOfYard = a;
    }

    @Override
    public String toString() {
        return "Cottage :\n"
                + " Price = " + price
                + "\n Range to University = "
                + rangeToUniversity
                + " \n Area = " + area
                + "\n Area of Yard = "
                + areaOfYard;
    }


    public int getAreaOfYard() {
        return areaOfYard;
    }
}
