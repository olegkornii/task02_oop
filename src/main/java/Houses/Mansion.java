package Houses;
/**
 * Represent kind of House
 * Have unique characteristic: Number of floors
 */
public class Mansion extends House {
    /**
     * Numbers floors of the mansion
     */
    private int numberOfFloors;

    public Mansion(int p, int r, int n, int s) {
        super.price = p;
        super.rangeToUniversity = r;
        super.area = s;
        numberOfFloors = n;
    }

    @Override
    public String toString() {
        return "Mansion \n"
                + " Number Of Floors = " + numberOfFloors
                + ",\n Price = " + price
                + ",\n Range To University = "
                + rangeToUniversity
                + ",\n Area = " + area;
    }

    public int getNumberOfFloors() {
        return numberOfFloors;
    }

}
