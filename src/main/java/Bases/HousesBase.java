package Bases;

import Houses.Apartment;
import Houses.Cottage;
import Houses.House;
import Houses.Mansion;

import java.util.ArrayList;
import java.util.List;

/**
 * Represent a base of houses.
 * Houses can be sorted by characteristics.
 * Houses can be sorted on Apartment, Mansion and Cottage
 */
public class HousesBase {
    /**
     * Represent a list of all houses
     */
    private static ArrayList<House> houseArrayList = new ArrayList<House>();
    /**
     * Represent a list of sorted houses
     */
    private List<House> sortedHouses = new ArrayList<House>();

    /**
     * Add house to base of houses
     *
     */
    public void addToBase(House house) {
        houseArrayList.add(house);
    }

    /**
     *
     * @return all houses from the base
     */
    public List<House> getAll() {

        return houseArrayList;
    }

    /**
     *
     * @param list The list of houses
     * @param p maximum price of house
     * @param r maximum range from house to university
     * @param area minimum area of house
     * @return all houses with suitable characteristics
     */
    public List<? extends House> sort(List<? extends House> list,
                                      int p, int r, int area) {

        for (House h
                : list) {
            if (p >= h.getPrice()
                    && r >= h.getRangeToUniversity()
                    && area <= h.getArea()) {
                sortedHouses.add(h);
            }
        }
        return sortedHouses;
    }

    /**
     *
     * @param list of apartments
     * @param price maximum price of house
     * @param range maximum range from house to university
     * @param area minimum area of house
     * @param floor maximum floor where apartment are locate
     * @return all apartments with suitable characteristics
     */
    public List<House> sortApartment(List<Apartment> list, int price, int range,
                                     int area, int floor) {
        if (price != 0
                && range != 0
                && area != 0
                && floor != 0) {
            for (Apartment a
                    : list) {
                if (price >= a.getPrice()
                        && range >= a.getRangeToUniversity()
                        && area <= a.getArea()
                        && floor >= a.getFloor()) {
                    sortedHouses.add(a);
                }
            }
        }
        return sortedHouses;
    }

    /**
     *
     * @param list of mansions
     * @param price maximum price of house
     * @param range maximum range to university
     * @param area minimum area of mansion
     * @param numberOfFlors minimum number mansion floors
     * @return all mansions with suitable characteristics
     */
    public List<House> sortMansion(List<Mansion> list, int price,
                                   int range, int area, int numberOfFlors) {
        if (price != 0
                && range != 0
                && area != 0
                && numberOfFlors != 0) {
            for (Mansion m
                    : list) {
                if (price >= m.getPrice()
                        && range >= m.getRangeToUniversity()
                        && area <= m.getArea()
                        && numberOfFlors <= m.getNumberOfFloors()) {
                    sortedHouses.add(m);
                }
            }
        }
        return sortedHouses;
    }

    /**
     *
     * @param list of cottages
     * @param price maximum price of cottage
     * @param range maximum range to university
     * @param area minimum area of cottage
     * @param areaOfYard minimum area of the yard
     * @return all cottages with suitable characteristics
     */
    public List<House> sortCottage(List<Cottage> list, int price,
                                   int range, int area, int areaOfYard) {

            for (Cottage c
                    : list) {
                if (price >= c.getPrice()
                        && range >= c.getRangeToUniversity()
                        && area >= c.getArea()
                        && areaOfYard <= c.getAreaOfYard()) {
                    sortedHouses.add(c);
                }
            }

        return sortedHouses;
    }

    /**
     * @return list of all apartments
     */
    public List<Apartment> getAllApartment() {
        List<Apartment> apartmentsList = new ArrayList<Apartment>();
        for (House house
                : houseArrayList) {
            if (house instanceof Apartment) {
                Apartment apartment = (Apartment) house;
                apartmentsList.add(apartment);
            }
        }
        return apartmentsList;
    }

    /**
     * @return list of all mansions
     */
    public List<Mansion> getAllMansions() {
        List<Mansion> mansionList = new ArrayList<Mansion>();
        for (House house
                : houseArrayList) {
            if (house instanceof Mansion) {
                Mansion mansion = (Mansion) house;
                mansionList.add(mansion);
            }
        }
        return mansionList;
    }

    /**
     * @return list of all cottages
     */
    public List<Cottage> getAllCottages() {
        List<Cottage> cottageList = new ArrayList<Cottage>();
        for (House house
                : houseArrayList) {
            if (house instanceof Cottage) {
                Cottage cottage = (Cottage) house;
                cottageList.add(cottage);

            }
        }
        return cottageList;
    }

    /**
     * Print in console list of houses
     * @param list of houses
     */
    public static void show(List<? extends House> list) {
        if (list.isEmpty()) {
            System.out.println("Немає підходящих варіантів");
        }
        StringBuilder sb = new StringBuilder();
        sb.append("_________________________\n");
        for (House h
                : list) {
            sb.append(h);
            sb.append("\n______________________\n").append(' ');

        }
        System.out.println(sb);

    }


}
