package Performence;

import Bases.HousesBase;

import java.util.Scanner;

/**
 * Perform introduction with user
 * Represent main scene of action
 */
public class Scene {
    private HousesBase housesBase = new HousesBase();
    private Performence performence = new Performence();

    /**
     * get information from user, what are he looking for
     */
    public final void chooseVariant() {

        int houseTypeChoice;
        System.out.println("Що ви шукаєте? ");
        System.out.println("_______________");
        System.out.println("Apartment    1*");
        System.out.println("Mansion      2*");
        System.out.println("Cottage      3*");
        System.out.println("All options  4*");


        do {

            houseTypeChoice = performence.criteriaEnter("Зробіть свій вибір");

        }while (houseTypeChoice !=1
                && houseTypeChoice !=2
                && houseTypeChoice !=3
                && houseTypeChoice !=4);
        switch (houseTypeChoice) {
            case 1: {

                performence.criterionChoise(housesBase.getAllApartment());
                break;

            }
            case 2: {

                performence.criterionChoise(housesBase.getAllMansions());
                break;
            }
            case 3: {
                performence.criterionChoise(housesBase.getAllCottages());
                break;
            }
            case 4: {
                performence.criterionChoise(housesBase.getAll());
                break;
            }

        }

    }
}
