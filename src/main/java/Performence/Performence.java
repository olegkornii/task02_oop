package Performence;

import Bases.HousesBase;
import Houses.Apartment;
import Houses.Cottage;
import Houses.House;
import Houses.Mansion;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import static Bases.HousesBase.*;


/**
 * Perform a interaction with user
 */
class Performence {

    private HousesBase housesBase = new HousesBase();

    /**
     * Interact with user
     * Get information from user
     *
     * @param list of houses
     */
    private void startPerformence(List<? extends House> list) {

        int price = criteriaEnter("Введіть ціну (Не більше ніж)");
        int range = criteriaEnter("Введіть відстань (Не більше ніж) ");
        int area = criteriaEnter("Введіть площу (Не менше ніж)");
        int unique;

        if (list.get(0) instanceof Apartment
                && list.get(1) instanceof Apartment) {

            unique = criteriaEnter("Введіть поверх (Не вище ніж)");
            System.out.println("Пропоновані варіанти:");
            show(housesBase.sortApartment((List<Apartment>) list,
                    price, range, area, unique));

        } else if (list.get(0) instanceof Mansion) {

            unique = criteriaEnter("Ведіть кількість поверхів (Не менше ніж)");
            System.out.println("Пропоновані варіанти:");
            show(housesBase.sortMansion((List<Mansion>) list,
                    price, range, area, unique));

        } else if (list.get(0) instanceof Cottage) {

            unique = criteriaEnter("Введіть площу подвір'я (Не менше ніж)");
            System.out.println("Пропоновані варіанти:");
            show(housesBase.sortCottage((List<Cottage>) list,
                    price, range, area, unique));

        } else {

            System.out.println("Пропоновані варіанти:");
            show(housesBase.sort(list, price, range, area));
        }

    }

    /**
     * Interact with user
     * Get information from user
     * Show all variants or call startPerformence()
     * @param list of houses
     */
    void criterionChoise(List<? extends House> list) {
        int choise;
        System.out.println("Подивитися всі варіанти 1*");
        System.out.println("Сортувати за критеріями 2*");

        do {
            choise = criteriaEnter("Зробіть свій вибір");
        }while (choise != 1 && choise !=2);
        switch (choise) {
            case 1: {
                show(list);
                break;
            }
            case 2: {
                startPerformence(list);
                break;
            }
        }
    }

    /**
     *
     * @param massage to user, that inform, what information
     *                he should enter
     * @return value, that user has introduced
     */
     int criteriaEnter(String massage) {
        int value = 0;
        do {

            try {
                System.out.println("____________");
                System.out.println(massage);
                Scanner scanner = new Scanner(System.in);
                value = scanner.nextInt();
                if (value <= 0) {
                    throw new InputMismatchException();
                }

            } catch (InputMismatchException e) {
                System.out.println("__________________________"
                        + "________________");
                System.out.println("Ви ввели не коректні дані,"
                        + " пробуйте ще раз");

            }
        } while (value <= 0);
        return value;
    }
}

